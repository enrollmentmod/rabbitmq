package com.example.demo.rabbitmq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("app")
public class RabbitMQWebController {

	@Autowired
	RabbitMQSender rabbitMQSender;

	@GetMapping(value = "/hii")
public void print()
{
	System.out.println("hello");
}
	@GetMapping(value = "/producer")
	 @Transactional
	public String producer(@RequestParam("empName") String empName,@RequestParam("empId") String empId) {
	System.out.println("Hello ");
	Employee emp=new Employee();
	emp.setEmpId(empId);
	emp.setEmpName(empName);
		rabbitMQSender.send(emp);
		return "Message sent to the RabbitMQ JavaInUse Successfully";
	}

}
